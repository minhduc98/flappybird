import React, {Component} from 'react';
import {View} from 'react-native';
import Image from 'react-native-fast-image';
import R from '../assets/R';

class Pipe extends Component {
  render() {
    const {body} = this.props;
    const width = body.bounds.max.x - body.bounds.min.x;
    const height = body.bounds.max.y - body.bounds.min.y;
    const x = body.position.x - width / 2;
    const y = body.position.y - height / 2;
    const container = {
      position: 'absolute',
      top: y,
      left: x,
      width,
      height,
      overflow: 'hidden',
      flexDirection: 'column',
    };

    const pipeRatio = 160 / width;
    const pipeHeight = 33 * pipeRatio;
    const pipeIterations = Math.ceil(height / pipeHeight);

    return (
      <View style={container}>
        {Array.apply(null, Array(pipeIterations)).map((value, index) => {
          return (
            <Image
              source={R.images.pipe_core}
              style={{width: width, height: pipeHeight}}
              resizeMode={Image.resizeMode.stretch}
              key={index}
            />
          );
        })}
      </View>
    );
  }
}

export default Pipe;
