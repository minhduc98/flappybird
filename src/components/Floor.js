import React, {Component} from 'react';
import {View} from 'react-native';
import Image from 'react-native-fast-image';
import R from '../assets/R';

class Floor extends Component {
  render() {
    const {body} = this.props;
    const width = body.bounds.max.x - body.bounds.min.x;
    const height = body.bounds.max.y - body.bounds.min.y;
    const x = body.position.x - width / 2;
    const y = body.position.y - height / 2;

    const imgIterations = Math.ceil(width / height);

    const container = {
      position: 'absolute',
      top: y,
      left: x,
      overflow: 'hidden',
      flexDirection: 'row',
      width,
      height,
    };
    return (
      <View style={container}>
        {Array.apply(null, Array(imgIterations)).map((value, index) => {
          return (
            <Image
              source={R.images.floor}
              style={{width: height, height}}
              resizeMode={Image.resizeMode.stretch}
              key={index}
            />
          );
        })}
      </View>
    );
  }
}

export default Floor;
