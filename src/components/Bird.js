import React, {Component} from 'react';
import {Animated, StyleSheet} from 'react-native';
import Image from 'react-native-fast-image';
import R from '../assets/R';
import {constants} from '../configs';

class Bird extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.animatedValue = new Animated.Value(this.props.body.velocity.y);
  }

  returnImage = (index) => {
    switch (index) {
      case 1:
        return R.images.bird1;
      case 2:
        return R.images.bird2;
      case 3:
        return R.images.bird3;
      default:
        return null;
    }
  };

  render() {
    const {body, pose} = this.props;
    const width = body.bounds.max.x - body.bounds.min.x;
    const height = body.bounds.max.y - body.bounds.min.y;
    const x = body.position.x - width / 2;
    const y = body.position.y - height / 2;
    this.animatedValue.setValue(body.velocity.y);
    let angle = this.animatedValue.interpolate({
      inputRange: [-10, 0, 10, 20],
      outputRange: ['-20deg', '0deg', '15deg', '45deg'],
      extrapolate: 'clamp',
    });
    let container = {
      position: 'absolute',
      justifyContent: 'center',
      top: y,
      left: x,
      width,
      height,
      transform: [{rotate: angle}],
    };
    const image = this.returnImage(pose);
    return (
      <Animated.View style={container}>
        <Image
          style={styles.img}
          resizeMode={Image.resizeMode.contain}
          source={image}
        />
      </Animated.View>
    );
  }
}

export default Bird;

const styles = StyleSheet.create({
  img: {
    width: constants.BIRD_WIDTH,
    height: constants.BIRD_HEIGHT,
  },
});
