import React, {Component} from 'react';
import Image from 'react-native-fast-image';
import R from '../assets/R';

class PipeTop extends Component {
  render() {
    const {body} = this.props;
    const width = body.bounds.max.x - body.bounds.min.x;
    const height = body.bounds.max.y - body.bounds.min.y;
    const x = body.position.x - width / 2;
    const y = body.position.y - height / 2;
    let container = {
      position: 'absolute',
      top: y,
      left: x,
      width,
      height,
    };
    return (
      <Image
        style={container}
        resizeMode={Image.resizeMode.stretch}
        source={R.images.pipe_top}
      />
    );
  }
}

export default PipeTop;
