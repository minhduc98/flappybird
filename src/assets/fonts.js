const fonts = {
  Roboto: 'Roboto',
  RobotoMedium: 'Roboto-Medium',
  RobotoRegular: 'Roboto-Regular',
  RobotoLightItalic: 'Roboto-LightItalic',
};
export default fonts;
