const images = {
  background: require('./images/background.png'),
  floor: require('./images/floor.png'),
  pipe_core: require('./images/pipe_core.png'),
  pipe_top: require('./images/pipe_top.png'),
  bird1: require('./images/bird1.png'),
  bird2: require('./images/bird2.png'),
  bird3: require('./images/bird3.png'),
};

export default images;
