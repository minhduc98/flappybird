import {Dimensions} from 'react-native';

const Constants = {
  MAX_WIDTH: Dimensions.get('window').width,
  MAX_HEIGHT: Dimensions.get('window').height,
  PIPE_WIDTH: 80,
  GAP_SIZE: 220,
  BIRD_WIDTH: 50,
  BIRD_HEIGHT: 41,
};

export default Constants;
