import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Text,
} from 'react-native';
import Image from 'react-native-fast-image';
import {GameEngine} from 'react-native-game-engine';
import Matter from 'matter-js';
import R from '../assets/R';
import {constants, Physics, HEIGHT, resetPipes} from '../configs';
import Bird from '../components/Bird';
import Floor from '../components/Floor';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      running: true,
      score: 0,
    };
    this.gameEngine = null;
    this.entities = this.setUpWorld();
  }

  setUpWorld = () => {
    let engine = Matter.Engine.create({
      enableSleeping: false,
    });
    let world = engine.world;
    world.gravity.y = 0.0;

    let bird = Matter.Bodies.rectangle(
      constants.MAX_WIDTH / 2,
      constants.MAX_HEIGHT / 2,
      constants.BIRD_WIDTH,
      constants.BIRD_HEIGHT,
    );
    let floor1 = Matter.Bodies.rectangle(
      constants.MAX_WIDTH / 2,
      constants.MAX_HEIGHT + HEIGHT(50),
      constants.MAX_WIDTH + 4,
      HEIGHT(50),
      {isStatic: true},
    );
    let floor2 = Matter.Bodies.rectangle(
      constants.MAX_WIDTH + constants.MAX_WIDTH / 2,
      constants.MAX_HEIGHT + HEIGHT(50),
      constants.MAX_WIDTH + 4,
      HEIGHT(50),
      {isStatic: true},
    );

    Matter.World.add(world, [bird, floor1, floor2]);
    Matter.Events.on(engine, 'collisionStart', (event) => {
      let pairs = event.pairs;
      this.gameEngine.dispatch({type: 'game-over'});
    });

    return {
      physics: {
        engine,
        world,
      },
      bird: {
        body: bird,
        pose: 2,
        renderer: Bird,
      },
      floor1: {
        body: floor1,
        renderer: Floor,
      },
      floor2: {
        body: floor2,
        renderer: Floor,
      },
    };
  };

  onEvent = (e) => {
    if (e.type === 'game-over') {
      this.setState({running: false});
    } else if (e.type === 'score') {
      this.setState({score: this.state.score + 1});
    }
  };

  reset = () => {
    resetPipes();
    this.gameEngine.swap(this.setUpWorld());
    this.setState({running: true, score: 0});
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="transparent"
          translucent
          showHideTransition="slide"
        />
        <Image
          source={R.images.background}
          style={styles.backgroundImage}
          resizeMode={Image.resizeMode.stretch}
        />
        <GameEngine
          ref={(ref) => {
            this.gameEngine = ref;
          }}
          style={styles.gameContainer}
          running={this.state.running}
          onEvent={this.onEvent}
          systems={[Physics]}
          entities={this.entities}
        />
        <Text style={styles.score}>{this.state.score}</Text>
        {!this.state.running && (
          <TouchableOpacity onPress={this.reset} style={styles.fullScreen}>
            <Text style={styles.txtGameOver}>Game Over</Text>
            <Text
              style={[
                styles.txtGameOver,
                {fontSize: 25, fontWeight: 'normal'},
              ]}>
              Try Again
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.colors.white,
  },
  gameContainer: {
    ...StyleSheet.absoluteFill,
  },
  backgroundImage: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  fullScreen: {
    flex: 1,
    backgroundColor: R.colors.black50p,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtGameOver: {
    fontSize: 40,
    color: R.colors.white,
    fontWeight: 'bold',
  },
  score: {
    position: 'absolute',
    color: R.colors.white,
    alignSelf: 'center',
    fontSize: 70,
    top: HEIGHT(40),
  },
});
